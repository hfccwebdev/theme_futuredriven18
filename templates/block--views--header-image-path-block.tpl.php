<?php

/**
 * @file
 * Theme implementation for header_background views block.
 *
 * @see block.tpl.php
 * @see template_preprocess()
 * @see template_preprocess_block()
 * @see template_process()
 *
 * @ingroup themeable
 */
?>
<?php print $content ?>

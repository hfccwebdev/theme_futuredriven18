<?php

/**
 * @file
 * Default theme implementation for a single paragraph item.
 *
 * Available variables:
 * - $content: An array of content items. Use render($content) to print them
 *   all, or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. By default the following classes are available, where
 *   the parts enclosed by {} are replaced by the appropriate values:
 *   - entity
 *   - entity-paragraphs-item
 *   - paragraphs-item-{bundle}
 *
 * Other variables:
 * - $classes_array: Array of html class attribute values. It is flattened into
 *   a string within the variable $classes.
 *
 * @see template_preprocess()
 * @see template_preprocess_entity()
 * @see template_process()
 */

$url = file_create_url($content['field_para_photos']['#items'][0]['uri']);
$inline_style = "background-image: url($url)";

?>
<div class="highlight-item <?php print $classes; ?>"<?php print $attributes; ?> style="<?php print $inline_style; ?>">
  <div class="content"<?php print $content_attributes; ?>>
    <span class="highlight-link no-link"><?php print render($content['field_para_link']); ?></span>
    <div class="highlight-wrap">
      <div class="highlight-title"><?php print render($content['field_para_long_text']); ?></div>
    </div>
  </div>
</div>

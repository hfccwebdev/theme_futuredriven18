<?php

/**
 * @file
 * Main header_background view template.
 *
 * @see views-view.tpl.php
 * @ingroup views_templates
 */
?>
<div class="slides-master-header-container">
  <?php if ($rows) { print $rows; }?>
</div>

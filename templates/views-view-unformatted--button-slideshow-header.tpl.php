<?php

/**
 * @file
 * Simple view template to display header_background view rows.
 *
 * @ingroup views_templates
 */
?>

<?php foreach ($rows as $id => $row): ?>
  <div class="slide">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>

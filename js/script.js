/* Misc small scripts for site functionality */
jQuery(document).ready(function ($) {
  /* Add dynamic labels to block elements */
  function addHiddenLabel() {
    var blockTitle = $(this).children("h2").first();
    if (blockTitle.text() != null && blockTitle.text() != undefined && blockTitle.text() != "") {
      var id = $(this).attr("id");
      id += "-label";
      blockTitle.attr("id", id);
      blockTitle.addClass("hidden");
      $(this).attr("aria-labelledby", id);
    }
    //else {
    //$(this).attr('aria-hidden', true); Enable this after siteimprove has checked blocks and youre 100% sure every visible block has an appropriate header see issue 6422
    //}
  }

  $(".sidebar .block").each(addHiddenLabel);
  $("#top-bar .block").each(addHiddenLabel);
  $("#billboard .block").each(addHiddenLabel);
});

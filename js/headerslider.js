jQuery(document).ready(function ($) {
  var slidesLength = $(".slides-master-header-container > .slide").length;
  slidesLength = slidesLength - 1; //removing 1 because our index starts with 0.
  var masterHeader = $(".front .slides-master-header-container");

  /* Ensuring billboard is height of caption */
  function billboardSize() {
    var min_height = $(".slide.active .header-image").outerHeight();
    $(".slides-master-header-container").css("min-height", min_height);
    $(".slide.active .header-image").css("min-height", min_height);
    $(".slide-nav").css("min-height", min_height);
  }

  function setNav() {
    var dataIndex = parseInt($(".slide.active").attr("data-index"));
    var nextIndex = dataIndex + 1;
    var prevIndex = dataIndex - 1;

    if (nextIndex > slidesLength) {
      nextIndex = 0;
    }
    if (prevIndex === -1) {
      prevIndex = slidesLength;
    }

    // build Next button
    var nextTitle = $(".slide[data-index='" + nextIndex + "'")
      .find("h2")
      .html();
    nextTitle = '<span class="hidden" >Next: ' + nextTitle + "</span>";
    $("#next").html(nextTitle);

    // build Prev button
    var prevTitle = $(".slide[data-index='" + prevIndex + "'")
      .find("h2")
      .html();
    prevTitle = '<span class="hidden" >Next: ' + prevTitle + "</span>";
    $("#prev").html(prevTitle);

    // set slide attributes
    $(".slide a").attr("tabindex", "-1");
    $(".slide").attr("aria-hidden", "true");
    $(".slide.active").attr("tabindex", "0");
    $(".slide.active").attr("aria-hidden", "false");
    $(".slide.active a").attr("tabindex", "0");
  }

  /* Build slider nav*/
  function buildNav(i) {
    masterHeader.find(".slide").each(function () {
      $(this).attr("tabindex", "-1");
      $(this).attr("aria-hidden", "true");
      $(this).attr("data-index", i);
      i++;
    });

    var navLabeledBy = $('<h3 id="billboard-navigation" class="hidden">Billboard Carousel Navigation</h3>');
    var nav = $('<nav class="slide-nav" aria-labelledby="billboard-navigation"></nav>');
    var navNextItem = $('<button class="nav-item" id="next"></button>');
    var navPrevItem = $('<button class="nav-item" id="prev"></button>');

    masterHeader.find(".slide").first().addClass("active");
    nav.append(navPrevItem);
    nav.append(navNextItem);
    masterHeader.after(nav);
    masterHeader.append(navLabeledBy);

    setNav();
  }

  function clickButton(change) {
    var activeSlide = $(".slide.active").first();
    var dataIndex = activeSlide.attr("data-index");
    dataIndex = parseInt(dataIndex);
    var targetIndex = dataIndex + change;

    // Handling for last and first slides, continuous rotation
    if (targetIndex > slidesLength) {
      targetIndex = 0;
    } else if (targetIndex === -1) {
      targetIndex = slidesLength;
    }
    var targetSlide = $(".slide[data-index='" + targetIndex + "'");

    // Hiding newly inactive slide from tab order
    activeSlide.removeClass("active");
    targetSlide.addClass("active");

    setNav();
    billboardSize();
  }

  $(window).resize(billboardSize);
  buildNav(0);
  billboardSize();

  // nav button click switch slides
  $("#next").click(function () {
    clickButton(1);
  });
  $("#prev").click(function () {
    clickButton(-1);
  });
});

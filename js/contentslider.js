jQuery(document).ready(function ($) {
  if ($(".content-slider-master-wrapper").length) {
    function removeButtonsMobile() {
      if (window.innerWidth <= "900") {
        $(".content-slider-radio-navigation").css("display", "none");
      } else {
        $(".content-slider-radio-navigation").css("display", "flex");
      }
    }

    $(".content-slider-master-wrapper").each(function (numSliders) {
      $(this).attr("id", "content-slider-" + (numSliders + 1) + "");
      var slideId = $(this).attr("id");
      var slideIdP = "#" + $(this).attr("id");
      console.log(slideId);
      var sliderTitleNum = "content slider #" + (numSliders + 1) + "";

      var slidesLength = $(slideIdP + " .slider-container > .slider").length;

      slidesLength = slidesLength - 1;

      var masterHeader = $(slideIdP + " .content-slider-wrapper");

      function setNav() {
        var dataIndex = parseInt($(slideIdP + " .slider.active").attr("data-index"));
        var nextIndex = dataIndex + 1;
        var prevIndex = dataIndex - 1;

        if (nextIndex > slidesLength) {
          nextIndex = 0;
        }
        if (prevIndex === -1) {
          prevIndex = slidesLength;
        }

        // set slide attributes
        $(slideIdP + " .slider").attr("tabindex", "-1");
        $(slideIdP + " .slider").attr("aria-hidden", "true");
        $(slideIdP + " .slider.active").attr("tabindex", "0");
        $(slideIdP + " .slider.active").attr("aria-hidden", "false");
        $(slideIdP + " .slider.active a").attr("tabindex", "0");

        if (
          $(slideIdP + " .slider")
            .last()
            .hasClass("active")
        ) {
          $(slideIdP + " .content-slider-next").css("display", "none");
          $(slideIdP + " .content-slider-next").attr("tabindex", "-1");
        } else {
          $(slideIdP + " .content-slider-next").css("display", "block");
          $(slideIdP + " .content-slider-next").attr("tabindex", "0");
        }

        if (
          $(slideIdP + " .slider")
            .first()
            .hasClass("active")
        ) {
          $(slideIdP + " .content-slider-prev").css("display", "none");
          $(slideIdP + " .content-slider-prev").attr("tabindex", "-1");
        } else {
          $(slideIdP + " .content-slider-prev").css("display", "block");
          $(slideIdP + " .content-slider-prev").attr("tabindex", "0");
        }
      }

      function buildNav(i) {
        masterHeader.find(".slider").each(function () {
          $(this).attr("tabindex", "-1");
          $(this).attr("aria-hidden", "true");
          $(this).attr("data-index", i);
          i++;
        });

        var navLabeledBy = $(
          '<h3 id="' +
            slideId +
            '-content-slider-navigation" class="hidden">Previous and/or next button navigation for ' +
            sliderTitleNum +
            "</h3>"
        );
        var nav = $(
          '<nav class="content-slider-navigation" aria-labelledby="' + slideId + '-content-slider-navigation"></nav>'
        );
        var navNextItem = $('<button class="content-slider-next">Next Slide</button>');
        var navPrevItem = $('<button class="content-slider-prev">Previous Slide</button>');
        var navLabeledByRadio = $(
          '<h3 id="' +
            slideId +
            '-content-slider-radio-navigation" class="hidden">Individual button slider navigation for ' +
            sliderTitleNum +
            "</h3>"
        );
        var radioNav =
          '<nav class="content-slider-radio-navigation" aria-labelledby="' + slideId + '-content-slider-radio-navigation">';

        $(slideIdP + " .slider-container > .slider").each(function (index, value) {
          radioNav +=
            '<button id="' +
            slideId +
            "-radionav-" +
            (index + 1) +
            '">' +
            "Slide number " +
            index +
            " for " +
            sliderTitleNum +
            "</button>";
        });

        radioNav += "</nav>";

        masterHeader.find(".slider").first().addClass("active");

        nav.append(navPrevItem);
        nav.append(navNextItem);
        masterHeader.after(navLabeledByRadio);
        masterHeader.after(radioNav);
        masterHeader.after(navLabeledBy);
        masterHeader.after(nav);
        $(slideIdP).find(".content-slider-radio-navigation > button").first().addClass("active");

        setNav();
      }

      function clickButton(change) {
        var activeSlide = $(slideIdP + " .slider.active").first();
        var dataIndex = activeSlide.attr("data-index");
        dataIndex = parseInt(dataIndex);
        var targetIndex = dataIndex + change;

        if (targetIndex > slidesLength) {
          targetIndex = 0;
        } else if (targetIndex === -1) {
          targetIndex = slidesLength;
        }

        var targetSlide = $(slideIdP + ' .slider[data-index="' + targetIndex + '"');

        // Hiding newly inactive slide from tab order
        activeSlide.removeClass("active");
        targetSlide.addClass("active");

        if (
          $(slideIdP + " .slider")
            .last()
            .hasClass("active")
        ) {
          $(slideIdP + " .content-slider-next").css("display", "none");
        } else {
          $(slideIdP + " .content-slider-next").css("display", "block");
        }

        if (
          $(slideIdP + " .slider")
            .first()
            .hasClass("active")
        ) {
          $(slideIdP + " .content-slider-prev").css("display", "none");
        } else {
          $(slideIdP + " .content-slider-prev").css("display", "block");
        }

        setNav();
        removeButtonsMobile();
      }

      /* Connect slides to navigation */
      $(slideIdP + " .content-slider-buttons > input").each(function (i) {
        $(this).attr("data-index", i + 1);
      });

      $(slideIdP + " .slider").each(function (i) {
        $(this).attr("data-index", i + 1);
      });

      $(slideIdP + " .slider-pagination label").each(function (i) {
        $(this).attr("aria-labelledby", "slide_" + (i + 1));
      });

      /* Slides */
      $(slideIdP + " .slider").each(function (i) {
        $(this).addClass("slide_" + (i + 1));
      });

      var slider = $(slideIdP + " .content-slider-wrapper");
      var container = $(slideIdP + " .slider-container");
      var nav = $(slideIdP + " .slider-pagination");
      var slide = $(container).children();
      var slidelength = $(slide).length;
      var slidewidth = $(slider).width() * slidelength;

      buildNav(0);

      // nav button click switch slides
      $(slideIdP + " .content-slider-next").click(function () {
        clickButton(1);
        $(slideIdP + " .slider-container").animate({ left: "-=100%" }, 400);

        setNav();
        var activeButton = $(slideIdP + " .content-slider-radio-navigation > button.active");
        $(activeButton).next().addClass("active");
        $(activeButton).removeClass("active");
      });

      //Change slides animations
      $(slideIdP + " .content-slider-prev").click(function () {
        clickButton(-1);
        $(slideIdP + " .slider-container").animate({ left: "+=100%" }, 400);
        setNav();
        var activeButton = $(slideIdP + " .content-slider-radio-navigation > button.active");
        $(activeButton).prev().addClass("active");
        $(activeButton).removeClass("active");
      });

      // Calc width of slides and animate based on slide+button relationship
      $(slideIdP + " .content-slider-radio-navigation > button").click(function (pos) {
        var pos = ($(this).index() * $(slider).width()) / 10;
        var responsivePos = Math.ceil(pos / 100) * 100;
        console.log(pos);
        console.log(responsivePos);
        $(container).animate({ left: "-" + responsivePos + "%" }, 400);

        var active_slide = $(slideIdP + " .slider.active");
        var target_slide_id = $(this).attr("id");
        target_slide_id = target_slide_id.replace(slideId + "-radionav-", "slide_");

        var target_slide = $(slideIdP + " .slider-container").find("." + target_slide_id);

        $(slideIdP + " .content-slider-radio-navigation > button").removeClass("active");
        $(this).addClass("active");
        active_slide.removeClass("active");
        target_slide.addClass("active");

        setNav();
      });

      $(window).resize(removeButtonsMobile);
      removeButtonsMobile();
    });
  }
});

jQuery(document).ready(function ($) {
  var words = $("#word-options").text();
  var words = words.split(", ");
  i = 0;
  // hiding region from screen readers due to rapid updates: https://developer.mozilla.org/en-US/docs/Web/Accessibility/ARIA/ARIA_Live_Regions
  $("#word-fill").attr("aria-live", "off");

  function wordChange() {
    $("#word-fill").fadeOut(function () {
      $(this)
        .html(words[(i = (i + 1) % words.length)])
        .fadeIn();
    });
  }

  setInterval(wordChange, 2000);
});

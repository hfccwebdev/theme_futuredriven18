jQuery(document).ready(function ($) {
  if ($(".button-slides-master-header-container").length) {
    // Ensuring billboard is height of caption
    function billboardSize() {
      var minHeight = $(".slide.active .header-image-content").outerHeight();
      //console.log(minHeight);
      $(".button-slides-master-header-container").css("min-height", minHeight);
      $(".slide.active .header-image").css("min-height", minHeight);
    }

    function setNav() {
      // set slide attributes
      $(".slide a").attr("tabindex", "-1");
      $(".slide.active a").attr("tabindex", "0");
      $(".slide").attr("aria-hidden", "true");
      $(".slide.active").attr("aria-hidden", "false");
    }

    // buildNav here
    function buildNav(i) {
      $(".button-slides-master-header-container")
        .find(".slide")
        .each(function () {
          $(this).attr("tabindex", "-1");
          $(this).attr("aria-hidden", "true");
          $(this).attr("data-index", i);
          i++;
        });
    }

    $(".button-slides-master-header-container").each(function () {
      var slidecontainer = $(this);
      var slide = $(this).find(".slide");
      var navLabeledBy = $('<h3 id="billboard-navigation" class="hidden">Billboard Carousel Navigation</h3>');
      var nav = '<nav class="button-slide-nav" aria-labelledby="billboard-navigation">';
      slide.each(function (index, value) {
        var slide_title = $(this).find(".header-image").find("h2").html();
        $(this).attr("id", "slide-" + index);
        nav += '<button class="slide-nav-item" id="nav-' + index + '">' + slide_title + "</button>";
      });

      nav += "</nav>";
      slidecontainer.append(navLabeledBy);
      slidecontainer.after(nav);
      $("#nav-0").addClass("active");

      // add first/last classes to respective first/last slides
      slide.last().addClass("last");
      slide.first().addClass("first active");

      setNav();
    });

    // nav button functionality
    $(".slide-nav-item").click(function () {
      var active_slide = $(".slide.active");
      var target_slide_id = $(this).attr("id");
      target_slide_id = target_slide_id.replace("nav-", "slide-");
      var target_slide = $(".button-slides-master-header-container").find("#" + target_slide_id);

      $(".slide-nav-item").removeClass("active");
      $(this).addClass("active");
      active_slide.removeClass("active");
      target_slide.addClass("active");
      target_slide.find(".billboard-caption-athletics a:first").focus();
      billboardSize();
      setNav();
    });

    $(window).resize(billboardSize);
    buildNav(0);
    setNav();
    billboardSize();
  }
});

jQuery(document).ready(function ($) {
  if ($(".menu-5101").length) {
    // Sticky Menu
    var stickyNavTop = $(".menu-5101").offset().top;
    var stickyNav = function () {
      var scrollTop = $(window).scrollTop();

      if (scrollTop > stickyNavTop) {
        $(".menu-5101").addClass("sticky horizontal-menu-5101");
      } else {
        $(".menu-5101").removeClass("sticky horizontal-menu-5101");
      }
    };

    $(".menu-5101 ul li a").addClass("smooth-scroll");

    // Smooth Scroll
    var hashTagActive = "";
    $(".smooth-scroll").click(function (event) {
      $(".menu-5101 li").removeClass("active");
      $(this).parent().addClass("active"); //adding active class to clicked li.

      if (hashTagActive != this.hash) {
        //this will prevent if the user click several times the same link to freeze the scroll.
        event.preventDefault();

        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname) {
          var target = $(this.hash);
          var headerHeight = $(".menu-5101.sticky").height() + 80;
          target = target.length ? target : $("[name=" + this.hash.slice(1) + "]");
          if (target.length) {
            $("html,body").animate(
              {
                scrollTop: target.offset().top - headerHeight,
              },
              800,
              "swing"
            );
            return false;
          }
        }
      }
    });

    stickyNav();

    $(window).scroll(function () {
      stickyNav();
    });
  }
});

jQuery(document).ready(function ($) {
  if ($(".para-nested-tab-wrapper").length) {
    var anchor = window.location.hash;

    $(".para-nested-tab-options ul li > a").first().addClass("active");
    $(".paragraphs-item-para-nested-tab").first().addClass("active");

    // set anchor offset on page load
    function offsetAnchor() {
      if (location.hash.length !== 0) {
        window.scrollTo(window.scrollX, window.scrollY - 100);
      }
    }

    window.addEventListener("hashchange", offsetAnchor);

    function activeTab() {
      $(".para-nested-tab-options ul li > a").each(function () {
        if (window.location.hash !== "" && $(this).attr("href").indexOf(anchor) !== -1) {
          $(".para-nested-tab-options ul li > a").removeClass("active");
          $(this).addClass("active");
        }
      });

      if (window.location.hash !== "") {
        $(".paragraphs-item-para-nested-tab").removeClass("active");
        $(".para-nested-tab-wrapper div" + anchor).addClass("active");
      }
    }

    activeTab();

    $(".paragraphs-item-para-nested-tab").each(function (i) {
      $(this).attr("data-index", "tab-" + i);
      $(this).addClass("tab-content");
    });

    $(".para-nested-tab-options ul li > a").each(function (i) {
      $(this).attr("data-index", "tab-" + i);
    });

    $(".para-nested-tab-options ul li > a").click(function () {
      var activeTabContent = $(".paragraphs-item-para-nested-tab.active");
      var targetTabAttr = $(this).attr("data-index");
      var targetTab = $('.tab-content[data-index="' + targetTabAttr + '"]');

      $(".para-nested-tab-options ul li > a").removeClass("active");
      $(this).addClass("active");
      activeTabContent.removeClass("active");
      targetTab.addClass("active");
    });

    /* Change tab if anchor link is clicked */

    $(".paragraphs-items a").click(function () {
      var anchor = $(this).attr("hash");
      if ($(this).hash !== "" && $(this).hash !== -1 && anchor.length > 0) {
        $(".paragraphs-item-para-nested-tab").removeClass("active");
        $(".para-nested-tab-wrapper > div" + anchor).addClass("active");
        $(".para-nested-tab-options ul li > a").removeClass("active");

        var targetTabAttr = $(this).attr("hash");
        var targetTab = $('a[href="' + targetTabAttr + '"');
        targetTab.addClass("active");
      }
    });

    window.setTimeout(function () {
      offsetAnchor();
    }, 0);
  }

  window.setTimeout(offsetAnchor, 0);
});

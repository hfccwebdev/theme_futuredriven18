<?php

/**
 * @file
 * This file provides theme override functions for the futuredriven18 theme.
 */

/**
 * Implements hook_template_preprocess_html().
 *
 * Preprocess variables for html.tpl.php.
 */
function futuredriven18_preprocess_html(&$variables) {

  // Add viewport metatag for mobile devices.
  $meta_viewport = array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1.0',
    ),
  );
  drupal_add_html_head($meta_viewport, 'meta_viewport');

  // Add Google Fonts.
  drupal_add_css('//fonts.googleapis.com/css?family=Open+Sans:400,400italic,700', array('group' => CSS_THEME, 'type' => 'external'));
  drupal_add_css('//fonts.googleapis.com/css?family=Work+Sans:400,500', array('group' => CSS_THEME, 'type' => 'external'));
  drupal_add_css('//fonts.googleapis.com/css?family=Graduate', array('group' => CSS_THEME, 'type' => 'external'));
  drupal_add_css('//fonts.googleapis.com/css?family=Roboto+Slab:400,700', array('group' => CSS_THEME, 'type' => 'external'));

  // Additional classes for body element.
  if (user_access('administer site configuration')) {
    $variables['classes_array'][] = 'admin';
  }

  if (empty($variables['page']['billboard'])) {
    $variables['classes_array'][] = 'no-billboard';
  }

  // Add classes for website section.
  if (!$variables['is_front']) {
    $path = drupal_get_path_alias($_GET['q']);
    if (arg(0) == 'node' && arg(1) == 'add') {
      $variables['classes_array'][] = 'section-node-add';
    }
    elseif (arg(0)== 'node' && is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
      $variables['classes_array'][] = 'section-node-' . arg(2); // Add 'section-node-edit' or 'section-node-delete'
    }
    else {
      list($section, ) = explode('/', $path, 2);
      $variables['classes_array'][] = futuredriven18_id_safe('section-' . $section);
    }
  }
}

/**
 * Implements hook_template_preprocess_page().
 */
function futuredriven18_preprocess_page(&$variables) {

  if ($destination = theme_get_setting('futuredriven18_logo_destination')) {
    $variables['front_page'] = url($destination);
  }
}

/**
 * Implements hook_page_alter().
 */
function futuredriven18_page_alter(&$variables) {
  if ($node = menu_get_object()) {
    if (isset($node->type) && ($node->type == 'marketing_landing_page')) {
      $variables['#notitle'] = TRUE;
    }
  }
}

/**
 * Implements hook_template_preprocess_block().
 *
 * Preprocess variables for block.tpl.php.
 */
function futuredriven18_preprocess_block(&$variables) {
  if (in_array($variables['block']->region, ['sidebar_first', 'sidebar_second'])) {
    array_unshift($variables['theme_hook_suggestions'], 'block__sidebar');
  }

  if (!empty($variables['block']->subject)) {

    $variables['title_attributes_array']['class'] = 'block-title';

    if ($variables['block']->module == 'menu') {
      // We had a major argument about this code.
      // See: https://www.w3.org/TR/wai-aria/#aria-label
      // and usage of aria-label vs aria-labelledby
      // $label ="block-{$variables['block']->delta}-label";
      // $variables['attributes_array']['aria-labelledby'] = $label;
      // $variables['title_attributes_array']['id'] = $label;

      $variables['attributes_array']['aria-label'] = t($variables['block']->subject);
    }
  }
}

/**
 * Implements hook_template_preprocess_node().
 *
 * Preprocess variables for node.tpl.php.
 */
function futuredriven18_preprocess_node(&$variables) {
  global $user;
  if ($variables['node']->uid && $variables['node']->uid == $user->uid) {
    $variables['classes_array'][] = 'node-mine';
  }
}

/**
 * Converts a string to a suitable html ID attribute.
 *
 * http://www.w3.org/TR/html4/struct/global.html#h-7.5.2 specifies what makes a
 * valid ID attribute in HTML. This function:
 *
 *   - Ensure an ID starts with an alpha character by optionally adding an 'n'.
 *   - Replaces any character except A-Z, numbers, and underscores with dashes.
 *   - Converts entire string to lowercase.
 *
 * @param $string
 *   The string.
 * @return
 *   The converted string.
 */
function futuredriven18_id_safe($string) {
  // Replace with dashes anything that isn't A-Z, numbers, dashes, or underscores.
  $string = drupal_strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  // If the first character is not a-z, add 'n' in front.
  if (!ctype_lower($string[0])) { // Don't use ctype_alpha since its locale aware.
    $string = 'id' . $string;
  }
  return $string;
}

/**
 * Implements hook_template_preprocess_entity().
 */
function futuredriven18_preprocess_entity(&$variables) {
  if ($variables['entity_type'] == 'bean' && !empty($variables['elements']['#bundle']) && $variables['elements']['#bundle'] == 'anywhere_button') {
    $imagevars = array('path' => $variables['field_button_image'][0]['uri']);
    if (!empty($variables['field_button_target'][0]['title'])) {
      $imagevars['alt'] = $variables['field_button_target'][0]['title'];
      $imagevars['title'] = $variables['field_button_target'][0]['title'];
    }
    $image = theme('image', $imagevars);
    $variables['content']['formatted_button'] = array(
       '#markup' =>l($image, $variables['field_button_target'][0]['url'], array('html' => TRUE)),
    );
    unset($variables['content']['field_button_target']);
    unset($variables['content']['field_button_image']);
  }
  elseif ($variables['entity_type'] == 'paragraphs_item') {
    $entity = $variables['elements']['#entity'];
    if (!empty($entity->field_para_background_photo[LANGUAGE_NONE][0]['fid'])) {
      $path = file_create_url($entity->field_para_background_photo[LANGUAGE_NONE][0]['uri']);
      $inline_style = "background-image: url($path)";
      $variables['attributes_array']['style'] = $inline_style;
    }
    if (!empty($entity->field_para_classes)) {
      // @todo: There is a core function for this!
      $classes = explode(' ', $entity->field_para_classes[LANGUAGE_NONE][0]['value']);
      foreach ($classes as $class) {
        $variables['classes_array'][] = drupal_html_class($class);
      }
    }
    if ($variables['elements']['#bundle'] == 'image_overlay_slide') {
      $content = &$variables['content'];

      // Rebuild content for image_overlay_slide.
      $output = [];

      $background_url = file_create_url($content['field_para_photos']['#items'][0]['uri']);
      $variables['attributes_array']['style'] = "background-image: url($background_url)";

      $output['overlay'] = [
        '#prefix' => '<div class="singleboxcontent" tabindex="0">',
        '#suffix' => '</div>',
      ];

      if (!empty($content['field_title'])) {
        $output['overlay'][] = $content['field_title'][0];
      }

      $output['overlay'][] = $content['field_para_long_text'][0];

      $content = $output;
      $variables['classes_array'][] = 'image-overlay-slide';
    }
    if ($variables['elements']['#bundle'] == 'content_slider') {
      $variables['classes_array'][] = 'content-slider-master-wrapper';
    }
    if ($variables['paragraphs_item']->hostEntityBundle() == 'content_slider') {
      $variables['classes_array'][] = 'slider';
    }
    if (!empty($variables['paragraphs_item']->field_para_nested_anchor)) {
      $variables['attributes_array']['id'] = $variables['paragraphs_item']->field_para_nested_anchor[LANGUAGE_NONE][0]['safe_value'];
    }
  }
}

/**
 * Implements hook_process_HOOK().
 */
function futuredriven18_process_entity(&$variables) {
  if ($variables['entity_type'] == 'paragraphs_item') {
    $item = $variables['paragraphs_item'];
    if (
      (
        !empty($item->field_news_release_date) &&
        (REQUEST_TIME < $item->field_news_release_date[LANGUAGE_NONE][0]['value'])
      ) || (
        !empty($item->field_news_retire_date) &&
        (REQUEST_TIME > $item->field_news_retire_date[LANGUAGE_NONE][0]['value'])
      )
    ) {
      $variables['content'] = '';
      $variables['theme_hook_suggestions'][] = 'paragraphs_item__unwrapped';
    }
  }
}

/**
 * Process variables for paragraphs-items.tpl.php
 */
function futuredriven18_preprocess_paragraphs_items(&$variables, $hook) {
  if ($variables['element']['#bundle'] == 'content_slider') {
    $variables['theme_hook_suggestions'][] = 'paragraphs_items__content_slider';
  }
  elseif ($variables['element']['#bundle'] == 'grid') {
    $entity = $variables['element']['#object'];
    $classes = $entity->field_para_classes[LANGUAGE_NONE][0]['value'] ?? NULL;
    if (preg_match('/has-tabs/', $classes)) {
      $tabs = [];
      foreach(element_children($variables['element']) as $id) {
        $tab = reset($variables['element'][$id]['entity']['paragraphs_item'])['#entity'];
        $tabs[] = t('<a href="#!url">!label</a>', [
          '!label' => $tab->field_para_nested_tab_label[LANGUAGE_NONE][0]['safe_value'],
          '!url' => $tab->field_para_nested_anchor[LANGUAGE_NONE][0]['safe_value'],
        ]);
      }
      $links = [
        '#prefix' => '<nav class="para-nested-tab-options">',
        '#theme' => 'item_list',
        '#items' => $tabs,
        '#suffix' => '</nav>',
      ];
      $content = render($links);
      $variables['content'] = '<div class="para-nested-tab-wrapper">' . $content . $variables['content'] . '</div>';
    }
  }
}

/**
 * Implements hook_template_preprocess_field().
 */
function futuredriven18_preprocess_field(&$variables, $hook) {

  // Specify alternate templates for selected fields.

  switch ($variables['element']['#field_name']) {
    case 'field_program_xfer_schools':
      $variables['theme_hook_suggestions'][] = 'field__list';
      $variables['field_list_type'] = 'ul';
      break;
  }

  $unwrapped = theme_get_setting('futuredriven18_unwrapped');
  if (!empty($unwrapped) && in_array($variables['element']['#field_name'], $unwrapped)) {
    $variables['theme_hook_suggestions'][] = 'field__unwrapped';
  }
}

/**
 * Implements hook_template_preprocess_username().
 *
 * @see realname_update()
 */
function futuredriven18_preprocess_username(&$variables) {

  if ($pattern = theme_get_setting('futuredriven18_realname_pattern')) {
    // $account = $variables['account'];
    $account = user_load($variables['account']->uid);
    // dpm($account, 'account');

    // Perform token replacement on the real name pattern.
    $realname = token_replace($pattern, array('user' => $account), array('clear' => TRUE, 'sanitize' => FALSE));

    // Remove any HTML tags.
    $realname = strip_tags(decode_entities($realname));

    // Remove double spaces (if a token had no value).
    $realname = preg_replace('/ {2,}/', ' ', $realname);

    // Trim the name to 255 characters.
    $realname = truncate_utf8(trim($realname), 255);

    // Substitute the real name if it is not empty, otherwise, leave the value unchanged.
    $variables['name'] = !empty($realname) ? $realname : $variables['name'];
  }
}

/**
 * Implements hook_template_preprocess_search_result().
 */
function futuredriven18_preprocess_search_result(&$variables) {
  // This removes the username and date/time info line from search results;
  $variables['info'] = NULL;
}

/**
 * Implements hook_form_alter()
 */
function futuredriven18_form_alter(&$form, &$form_state, $form_id) {
  if ($form_id == 'search_block_form') {

    $form['search_block_form']['#title_display'] = 'invisible';
    $form['search_block_form']['#size'] = 13;

    if (module_exists('google_cse')) {
        // Suppress Google CSE javascript.
        if (!empty($form['#attributes']['class'])) {
            foreach ($form['#attributes']['class'] as $key => $value){
              if ($value == 'google-cse') {
                unset($form['#attributes']['class'][$key]);
              }
            }
            if (empty($form['#attributes']['class'])) {
              unset($form['#attributes']['class']);
            }
        }

        // Suppress Google CSE field title.
        unset($form['search_block_form']['#attributes']['title']);

        // Suppress Google CSE default search value.
        $form['search_block_form']['#default_value'] = NULL;
    }
  }
}

/**
 * Implements date_all_day_label()
 * overrides 'all day' time display
 */
function futuredriven18_date_all_day_label() {
  return '';
}
